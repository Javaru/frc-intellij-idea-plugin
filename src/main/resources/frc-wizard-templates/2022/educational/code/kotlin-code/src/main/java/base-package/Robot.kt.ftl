<#ftl output_format="plainText" encoding="UTF-8">
<#--noinspection WrongPackageStatement-->
<#compress>
<#-- @ftlvariable name="data" type="net.javaru.iip.frc.wizard.FrcProjectWizardData" -->
<#--
    IMPORTANT: This tempolate uses alternate square bracket interpolation syntax
               For example:
                    [=data.robotClassSimpleName]
               rather than:
                    ${data.robotClassSimpleName}
               so as to not clash with Kotlin string templates syntax
               The option to use that can't be set in the template, but has to be set as an
               option on the Configuration object in the code. Note that this only affects interpolation
               syntax, and *NOT* Tag syntax. So we will still use `<#if isSuchAndSuch>` and not `[#if isSuchAndSuch]`.
               Tag syntax can be changed if desired, but we are not.
    Template Language Reference: https://freemarker.apache.org/docs/ref.html
    Template Author's Guide:     https://freemarker.apache.org/docs/dgui.html
-->
<#--  To DEBUG templates, set system property 'frc.freemarker.debug' to true when launching the testing instance of IntelliJ IDEA -->
</#compress>
package [=data.basePackage]

/**
 * The VM is configured to automatically run this object (which basically function as a singleton class),
 * and to call the [run] function when the robot is enabled. This is written as an object rather than a
 * class since there should only ever be a single instance, and it cannot take any constructor arguments.
 * This makes it a natural fit to be an object in Kotlin.
 *
 * If you change the name of this object or its package after creating this project, you must also update
 * the `Main.kt` file in the project. (If you use the IDE's Rename or Move refactorings when renaming the
 * object or package, it will get changed everywhere.)
 *
 * This robot extends EducationalRobot. Do NOT use for competitions.
 * This is a simple robot used for teaching purposes.
 */
object [=data.robotClassSimpleName] : EducationalRobot()
{
    /**
     * This method is run when the robot is first started up and should be
     * used for any initialization code.
     */
    override fun robotInit() {}

    /** This method is run when the robot is enabled.  */
    override fun run() {}
}