<#ftl output_format="plainText" encoding="UTF-8">
<#--noinspection WrongPackageStatement-->
<#compress>
<#-- @ftlvariable name="data" type="net.javaru.iip.frc.wizard.FrcProjectWizardData" -->
<#--
    Template Language Reference: https://freemarker.apache.org/docs/ref.html
    Template Author's Guide:     https://freemarker.apache.org/docs/dgui.html
-->
<#--  To DEBUG templates, set system property 'frc.freemarker.debug' to true when launching the testing instance of IntelliJ IDEA -->
</#compress>
${data.copyright}

package ${data.basePackage};

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import ${data.basePackage}.Constants.OIConstants;
import edu.wpi.first.wpilibj.shuffleboard.EventImportance;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;

import static edu.wpi.first.wpilibj.XboxController.Button;



/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer
{
    // The driver's controller
    private final XboxController driverController =
            new XboxController(OIConstants.DRIVER_CONTROLLER_PORT);
    
    // A few commands that do nothing, but will demonstrate the scheduler functionality
    private final CommandBase instantCommand1 = new InstantCommand();
    private final CommandBase instantCommand2 = new InstantCommand();
    private final CommandBase waitCommand = new WaitCommand(5);
    
    
    /** The container for the robot. Contains subsystems, OI devices, and commands. */
    public RobotContainer()
    {
        // Set names of commands
        instantCommand1.setName("Instant Command 1");
        instantCommand2.setName("Instant Command 2");
        waitCommand.setName("Wait 5 Seconds Command");
        
        // Set the scheduler to log Shuffleboard events for command initialize, interrupt, finish
        CommandScheduler.getInstance()
                        .onCommandInitialize(
                                command ->
                                        Shuffleboard.addEventMarker(
                                                "Command initialized", command.getName(), EventImportance.kNormal));
        CommandScheduler.getInstance()
                        .onCommandInterrupt(
                                command ->
                                        Shuffleboard.addEventMarker(
                                                "Command interrupted", command.getName(), EventImportance.kNormal));
        CommandScheduler.getInstance()
                        .onCommandFinish(
                                command ->
                                        Shuffleboard.addEventMarker(
                                                "Command finished", command.getName(), EventImportance.kNormal));
        
        // Configure the button bindings
        configureButtonBindings();
    }
    
    
    /**
     * Use this method to define your button->command mappings. Buttons can be created by
     * instantiating a {@link GenericHID} or one of its subclasses ({@link
     * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
     * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
     */
    private void configureButtonBindings()
    {
        // Run instant command 1 when the 'A' button is pressed
        new JoystickButton(driverController, Button.kA.value).whenPressed(instantCommand1);
        // Run instant command 2 when the 'X' button is pressed
        new JoystickButton(driverController, Button.kX.value).whenPressed(instantCommand2);
        // Run instant command 3 when the 'Y' button is held; release early to interrupt
        new JoystickButton(driverController, Button.kY.value).whenHeld(waitCommand);
    }
    
    
    /**
     * Use this to pass the autonomous command to the main {@link Robot} class.
     *
     * @return the command to run in autonomous
     */
    public Command getAutonomousCommand()
    {
        return new InstantCommand();
    }
}
