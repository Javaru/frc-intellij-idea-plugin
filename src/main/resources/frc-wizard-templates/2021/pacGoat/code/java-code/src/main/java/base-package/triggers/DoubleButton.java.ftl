<#ftl output_format="plainText" encoding="UTF-8">
<#--noinspection WrongPackageStatement,DanglingJavadoc-->
<#compress>
<#-- @ftlvariable name="data" type="net.javaru.iip.frc.wizard.FrcProjectWizardData" -->
<#--    
    Template Language Reference: https://freemarker.apache.org/docs/ref.html
    Template Author's Guide:     https://freemarker.apache.org/docs/dgui.html
-->
<#--  To DEBUG templates, set system property 'frc.freemarker.debug' to true when launching IntelliJ IDEA -->
</#compress>
${data.copyright}

package ${data.basePackage}.triggers;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Trigger;

/**
 * A custom button that is triggered when TWO buttons on a Joystick are
 * simultaneously pressed.
 */
public class DoubleButton extends Trigger
{
    private final Joystick joy;
    private final int button1;
    private final int button2;

    /**
     * Create a new double button trigger.
     *
     * @param joy     The joystick
     * @param button1 The first button
     * @param button2 The second button
     */
    public DoubleButton(Joystick joy, int button1, int button2)
    {
        this.joy = joy;
        this.button1 = button1;
        this.button2 = button2;
    }

    @Override
    public boolean get()
    {
        return joy.getRawButton(button1) && joy.getRawButton(button2);
    }
}
