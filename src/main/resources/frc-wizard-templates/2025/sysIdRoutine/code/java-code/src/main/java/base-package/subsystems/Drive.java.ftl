<#ftl output_format="plainText" encoding="UTF-8">
<#--noinspection WrongPackageStatement-->
<#compress>
<#-- @ftlvariable name="data" type="net.javaru.iip.frc.wizard.FrcProjectWizardData" -->
<#--
    Template Language Reference: https://freemarker.apache.org/docs/ref.html
    Template Author's Guide:     https://freemarker.apache.org/docs/dgui.html
-->
<#--  To DEBUG templates, set system property 'frc.freemarker.debug' to true when launching the testing instance of IntelliJ IDEA -->
</#compress>
${data.copyright}

package ${data.basePackage}.subsystems;

import java.util.function.DoubleSupplier;

import edu.wpi.first.units.measure.MutDistance;
import edu.wpi.first.units.measure.MutLinearVelocity;
import edu.wpi.first.units.measure.MutVoltage;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import ${data.basePackage}.Constants.DriveConstants;
import edu.wpi.first.wpilibj.motorcontrol.PWMSparkMax;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj2.command.sysid.SysIdRoutine;

import static edu.wpi.first.units.Units.*;



public class Drive extends SubsystemBase
{
    // The motors on the left side of the drive.
    private final PWMSparkMax leftMotor = new PWMSparkMax(DriveConstants.LEFT_MOTOR_1_PORT);
    
    // The motors on the right side of the drive.
    private final PWMSparkMax rightMotor = new PWMSparkMax(DriveConstants.RIGHT_MOTOR_1_PORT);
    
    // The robot's drive
    private final DifferentialDrive drive =
            new DifferentialDrive(leftMotor::set, rightMotor::set);
    
    // The left-side drive encoder
    private final Encoder leftEncoder =
            new Encoder(
                    DriveConstants.LEFT_ENCODER_PORTS[0],
                    DriveConstants.LEFT_ENCODER_PORTS[1],
                    DriveConstants.LEFT_ENCODER_REVERSED);
    
    // The right-side drive encoder
    private final Encoder rightEncoder =
            new Encoder(
                    DriveConstants.RIGHT_ENCODER_PORTS[0],
                    DriveConstants.RIGHT_ENCODER_PORTS[1],
                    DriveConstants.RIGHT_ENCODER_REVERSED);
    
    // Mutable holder for unit-safe voltage values, persisted to avoid reallocation.
    private final MutVoltage appliedVoltage = Volts.mutable(0);
    // Mutable holder for unit-safe linear distance values, persisted to avoid reallocation.
    private final MutDistance distance = Meters.mutable(0);
    // Mutable holder for unit-safe linear velocity values, persisted to avoid reallocation.
    private final MutLinearVelocity velocity = MetersPerSecond.mutable(0);
    
    // Create a new SysId routine for characterizing the drive.
    private final SysIdRoutine sysIdRoutine =
            new SysIdRoutine(
                    // Empty config defaults to 1 volt/second ramp rate and 7 volt step voltage.
                    new SysIdRoutine.Config(),
                    new SysIdRoutine.Mechanism(
                            // Tell SysId how to plumb the driving voltage to the motors.
                            voltage -> {
                                leftMotor.setVoltage(voltage);
                                rightMotor.setVoltage(voltage);
                            },
                            // Tell SysId how to record a frame of data for each motor on the mechanism being
                            // characterized.
                            log -> {
                                // Record a frame for the left motors.  Since these share an encoder, we consider
                                // the entire group to be one motor.
                                log.motor("drive-left")
                                   .voltage(
                                           appliedVoltage.mut_replace(
                                                   leftMotor.get() * RobotController.getBatteryVoltage(), Volts))
                                   .linearPosition(distance.mut_replace(leftEncoder.getDistance(), Meters))
                                   .linearVelocity(
                                           velocity.mut_replace(leftEncoder.getRate(), MetersPerSecond));
                                // Record a frame for the right motors.  Since these share an encoder, we consider
                                // the entire group to be one motor.
                                log.motor("drive-right")
                                   .voltage(
                                           appliedVoltage.mut_replace(
                                                   rightMotor.get() * RobotController.getBatteryVoltage(), Volts))
                                   .linearPosition(distance.mut_replace(rightEncoder.getDistance(), Meters))
                                   .linearVelocity(
                                           velocity.mut_replace(rightEncoder.getRate(), MetersPerSecond));
                            },
                            // Tell SysId to make generated commands require this subsystem, suffix test state in
                            // WPILog with this subsystem's name ("drive")
                            this));
    
    
    /** Creates a new Drive subsystem. */
    public Drive()
    {
        // Add the second motors on each side of the drivetrain
        leftMotor.addFollower(new PWMSparkMax(DriveConstants.LEFT_MOTOR_2_PORT));
        rightMotor.addFollower(new PWMSparkMax(DriveConstants.RIGHT_MOTOR_2_PORT));
        
        // We need to invert one side of the drivetrain so that positive voltages
        // result in both sides moving forward. Depending on how your robot's
        // gearbox is constructed, you might have to invert the left side instead.
        rightMotor.setInverted(true);
        
        // Sets the distance per pulse for the encoders
        leftEncoder.setDistancePerPulse(DriveConstants.ENCODER_DISTANCE_PER_PULSE);
        rightEncoder.setDistancePerPulse(DriveConstants.ENCODER_DISTANCE_PER_PULSE);
    }
    
    
    /**
     * Returns a command that drives the robot with arcade controls.
     *
     * @param fwd the commanded forward movement
     * @param rot the commanded rotation
     */
    public Command arcadeDriveCommand(DoubleSupplier fwd, DoubleSupplier rot)
    {
        // A split-stick arcade command, with forward/backward controlled by the left
        // hand, and turning controlled by the right.
        return run(() -> drive.arcadeDrive(fwd.getAsDouble(), rot.getAsDouble()))
                .withName("arcadeDrive");
    }
    
    
    /**
     * Returns a command that will execute a quasistatic test in the given direction.
     *
     * @param direction The direction (forward or reverse) to run the test in
     */
    public Command sysIdQuasistatic(SysIdRoutine.Direction direction)
    {
        return sysIdRoutine.quasistatic(direction);
    }
    
    
    /**
     * Returns a command that will execute a dynamic test in the given direction.
     *
     * @param direction The direction (forward or reverse) to run the test in
     */
    public Command sysIdDynamic(SysIdRoutine.Direction direction)
    {
        return sysIdRoutine.dynamic(direction);
    }
}
