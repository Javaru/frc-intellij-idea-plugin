<#ftl output_format="plainText" encoding="UTF-8">
<#--noinspection WrongPackageStatement-->
<#compress>
<#-- @ftlvariable name="data" type="net.javaru.iip.frc.wizard.FrcProjectWizardData" -->
<#--
    Template Language Reference: https://freemarker.apache.org/docs/ref.html
    Template Author's Guide:     https://freemarker.apache.org/docs/dgui.html
-->
<#--  To DEBUG templates, set system property 'frc.freemarker.debug' to true when launching the testing instance of IntelliJ IDEA -->
</#compress>
${data.copyright}

package ${data.basePackage};

import edu.wpi.first.math.Nat;
import edu.wpi.first.math.VecBuilder;
import edu.wpi.first.math.controller.LinearQuadraticRegulator;
import edu.wpi.first.math.estimator.KalmanFilter;
import edu.wpi.first.math.numbers.N1;
import edu.wpi.first.math.system.LinearSystem;
import edu.wpi.first.math.system.LinearSystemLoop;
import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.math.system.plant.LinearSystemId;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.motorcontrol.PWMSparkMax;



/**
 * This is a sample program to demonstrate how to use a state-space controller to control a
 * flywheel.
 */
public class ${data.robotClassSimpleName} extends TimedRobot
{
    private static final int MOTOR_PORT = 0;
    private static final int ENCODER_A_CHANNEL = 0;
    private static final int ENCODER_B_CHANNEL = 1;
    private static final int JOYSTICK_PORT = 0;
    private static final double SPINUP_RAD_PER_SEC = Units.rotationsPerMinuteToRadiansPerSecond(500.0);
    
    private static final double FLYWHEEL_MOMENT_OF_INERTIA = 0.00032; // kg * m^2
    
    // Reduction between motors and encoder, as output over input. If the flywheel spins slower than
    // the motors, this number should be greater than one.
    private static final double FLYWHEEL_GEARING = 1.0;
    
    // The plant holds a state-space model of our flywheel. This system has the following properties:
    //
    // States: [velocity], in radians per second.
    // Inputs (what we can "put in"): [voltage], in volts.
    // Outputs (what we can measure): [velocity], in radians per second.
    private final LinearSystem<N1, N1, N1> flywheelPlant =
            LinearSystemId.createFlywheelSystem(
                    DCMotor.getNEO(2), FLYWHEEL_MOMENT_OF_INERTIA, FLYWHEEL_GEARING);
    
    // The observer fuses our encoder data and voltage inputs to reject noise.
    private final KalmanFilter<N1, N1, N1> observer =
            new KalmanFilter<>(
                    Nat.N1(),
                    Nat.N1(),
                    flywheelPlant,
                    VecBuilder.fill(3.0), // How accurate we think our model is
                    VecBuilder.fill(0.01), // How accurate we think our encoder
                    // data is
                    0.020);
    
    // A LQR uses feedback to create voltage commands.
    private final LinearQuadraticRegulator<N1, N1, N1> controller =
            new LinearQuadraticRegulator<>(
                    flywheelPlant,
                    VecBuilder.fill(8.0), // QELMs. Velocity error tolerance, in radians per second. Decrease
                    // this to more heavily penalize state excursion, or make the controller behave more
                    // aggressively.
                    VecBuilder.fill(12.0), // RELMs. Control effort (voltage) tolerance. Decrease this to more
                    // heavily penalize control effort, or make the controller less aggressive. 12 is a good
                    // starting point because that is the (approximate) maximum voltage of a battery.
                    0.020); // Nominal time between loops. 0.020 for TimedRobot, but can be
    // lower if using notifiers.
    
    // The state-space loop combines a controller, observer, feedforward and plant for easy control.
    private final LinearSystemLoop<N1, N1, N1> loop =
            new LinearSystemLoop<>(flywheelPlant, controller, observer, 12.0, 0.020);
    
    // An encoder set up to measure flywheel velocity in radians per second.
    private final Encoder encoder = new Encoder(ENCODER_A_CHANNEL, ENCODER_B_CHANNEL);
    
    private final PWMSparkMax motor = new PWMSparkMax(MOTOR_PORT);
    
    // A joystick to read the trigger from.
    private final Joystick joystick = new Joystick(JOYSTICK_PORT);
    
    
    public Robot()
    {
        // We go 2 pi radians per 4096 clicks.
        encoder.setDistancePerPulse(2.0 * Math.PI / 4096.0);
    }
    
    
    @Override
    public void teleopInit()
    {
        loop.reset(VecBuilder.fill(encoder.getRate()));
    }
    
    
    @Override
    public void teleopPeriodic()
    {
        // Sets the target speed of our flywheel. This is similar to setting the setpoint of a
        // PID controller.
        if (joystick.getTriggerPressed())
        {
            // We just pressed the trigger, so let's set our next reference
            loop.setNextR(VecBuilder.fill(SPINUP_RAD_PER_SEC));
        }
        else if (joystick.getTriggerReleased())
        {
            // We just released the trigger, so let's spin down
            loop.setNextR(VecBuilder.fill(0.0));
        }
        
        // Correct our Kalman filter's state vector estimate with encoder data.
        loop.correct(VecBuilder.fill(encoder.getRate()));
        
        // Update our LQR to generate new voltage commands and use the voltages to predict the next
        // state with out Kalman filter.
        loop.predict(0.020);
        
        // Send the new calculated voltage to the motors.
        // voltage = duty cycle * battery voltage, so
        // duty cycle = voltage / battery voltage
        double nextVoltage = loop.getU(0);
        motor.setVoltage(nextVoltage);
    }
}
