<#ftl output_format="plainText" encoding="UTF-8">
<#--noinspection WrongPackageStatement-->
<#compress>
<#-- @ftlvariable name="data" type="net.javaru.iip.frc.wizard.FrcProjectWizardData" -->
<#--
    Template Language Reference: https://freemarker.apache.org/docs/ref.html
    Template Author's Guide:     https://freemarker.apache.org/docs/dgui.html
-->
<#--  To DEBUG templates, set system property 'frc.freemarker.debug' to true when launching the testing instance of IntelliJ IDEA -->
</#compress>
${data.copyright}

package ${data.basePackage};

import edu.wpi.first.math.Nat;
import edu.wpi.first.math.VecBuilder;
import edu.wpi.first.math.controller.LinearQuadraticRegulator;
import edu.wpi.first.math.estimator.KalmanFilter;
import edu.wpi.first.math.numbers.N1;
import edu.wpi.first.math.system.LinearSystem;
import edu.wpi.first.math.system.LinearSystemLoop;
import edu.wpi.first.math.system.plant.LinearSystemId;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.motorcontrol.PWMSparkMax;



/**
 * This is a sample program to demonstrate how to use a state-space controller to control a
 * flywheel.
 */
public class ${data.robotClassSimpleName} extends TimedRobot
{
    private static final int MOTOR_PORT = 0;
    private static final int ENCODER_A_CHANNEL = 0;
    private static final int ENCODER_B_CHANNEL = 1;
    private static final int JOYSTICK_PORT = 0;
    private static final double SPINUP_RAD_PER_SEC = Units.rotationsPerMinuteToRadiansPerSecond(500.0);
    
    // Volts per (radian per second)
    private static final double FLYWHEEL_KV = 0.023;
    
    // Volts per (radian per second squared)
    private static final double FLYWHEEL_KA = 0.001;
    
    // The plant holds a state-space model of our flywheel. This system has the following properties:
    //
    // States: [velocity], in radians per second.
    // Inputs (what we can "put in"): [voltage], in volts.
    // Outputs (what we can measure): [velocity], in radians per second.
    //
    // The Kv and Ka constants are found using the FRC Characterization toolsuite.
    private final LinearSystem<N1, N1, N1> flywheelPlant =
            LinearSystemId.identifyVelocitySystem(FLYWHEEL_KV, FLYWHEEL_KA);
    
    // The observer fuses our encoder data and voltage inputs to reject noise.
    private final KalmanFilter<N1, N1, N1> observer =
            new KalmanFilter<>(
                    Nat.N1(),
                    Nat.N1(),
                    flywheelPlant,
                    VecBuilder.fill(3.0), // How accurate we think our model is
                    VecBuilder.fill(0.01), // How accurate we think our encoder
                    // data is
                    0.020);
    
    // A LQR uses feedback to create voltage commands.
    private final LinearQuadraticRegulator<N1, N1, N1> controller =
            new LinearQuadraticRegulator<>(
                    flywheelPlant,
                    VecBuilder.fill(8.0), // Velocity error tolerance
                    VecBuilder.fill(12.0), // Control effort (voltage) tolerance
                    0.020);
    
    // The state-space loop combines a controller, observer, feedforward and plant for easy control.
    private final LinearSystemLoop<N1, N1, N1> loop =
            new LinearSystemLoop<>(flywheelPlant, controller, observer, 12.0, 0.020);
    
    // An encoder set up to measure flywheel velocity in radians per second.
    private final Encoder encoder = new Encoder(ENCODER_A_CHANNEL, ENCODER_B_CHANNEL);
    
    private final PWMSparkMax motor = new PWMSparkMax(MOTOR_PORT);
    
    // A joystick to read the trigger from.
    private final Joystick joystick = new Joystick(JOYSTICK_PORT);
    
    
    public Robot()
    {
        // We go 2 pi radians per 4096 clicks.
        encoder.setDistancePerPulse(2.0 * Math.PI / 4096.0);
    }
    
    
    @Override
    public void teleopInit()
    {
        // Reset our loop to make sure it's in a known state.
        loop.reset(VecBuilder.fill(encoder.getRate()));
    }
    
    
    @Override
    public void teleopPeriodic()
    {
        // Sets the target speed of our flywheel. This is similar to setting the setpoint of a
        // PID controller.
        if (joystick.getTriggerPressed())
        {
            // We just pressed the trigger, so let's set our next reference
            loop.setNextR(VecBuilder.fill(SPINUP_RAD_PER_SEC));
        }
        else if (joystick.getTriggerReleased())
        {
            // We just released the trigger, so let's spin down
            loop.setNextR(VecBuilder.fill(0.0));
        }
        
        // Correct our Kalman filter's state vector estimate with encoder data.
        loop.correct(VecBuilder.fill(encoder.getRate()));
        
        // Update our LQR to generate new voltage commands and use the voltages to predict the next
        // state with out Kalman filter.
        loop.predict(0.020);
        
        // Send the new calculated voltage to the motors.
        // voltage = duty cycle * battery voltage, so
        // duty cycle = voltage / battery voltage
        double nextVoltage = loop.getU(0);
        motor.setVoltage(nextVoltage);
    }
}
