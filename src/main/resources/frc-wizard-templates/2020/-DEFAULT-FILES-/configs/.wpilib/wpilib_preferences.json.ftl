<#ftl output_format="plainText" encoding="UTF-8">
<#compress>
<#-- @ftlvariable name="data" type="net.javaru.iip.frc.wizard.FrcProjectWizardData" -->
</#compress>
{
    "enableCppIntellisense": false,
    "currentLanguage": "java",
    "projectYear": "${data.projectYear}",
    "teamNumber": ${data.teamNumberAsString}
}