<#ftl output_format="plainText" encoding="UTF-8">
<#--noinspection WrongPackageStatement-->
<#compress>
<#-- @ftlvariable name="data" type="net.javaru.iip.frc.wizard.FrcProjectWizardData" -->
<#--
    Template Language Reference: https://freemarker.apache.org/docs/ref.html
    Template Author's Guide:     https://freemarker.apache.org/docs/dgui.html
-->
<#--  To DEBUG templates, set system property 'frc.freemarker.debug' to true when launching the testing instance of IntelliJ IDEA -->
</#compress>
${data.copyright}

package ${data.basePackage};

import edu.wpi.first.math.Nat;
import edu.wpi.first.math.VecBuilder;
import edu.wpi.first.math.controller.LinearQuadraticRegulator;
import edu.wpi.first.math.estimator.KalmanFilter;
import edu.wpi.first.math.numbers.N1;
import edu.wpi.first.math.numbers.N2;
import edu.wpi.first.math.system.LinearSystem;
import edu.wpi.first.math.system.LinearSystemLoop;
import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.math.system.plant.LinearSystemId;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import edu.wpi.first.wpilibj.motorcontrol.PWMSparkMax;



/**
 * This is a sample program to demonstrate how to use a state-space controller to control an arm.
 */
public class ${data.robotClassSimpleName} extends TimedRobot
{
    private static final int MOTOR_PORT = 0;
    private static final int ENCODER_A_CHANNEL = 0;
    private static final int ENCODER_B_CHANNEL = 1;
    private static final int JOYSTICK_PORT = 0;
    private static final double RAISED_POSITION = Units.degreesToRadians(90.0);
    private static final double LOWERED_POSITION = Units.degreesToRadians(0.0);
    
    // Moment of inertia of the arm, in kg * m^2. Can be estimated with CAD. If finding this constant
    // is difficult, LinearSystem.identifyPositionSystem may be better.
    private static final double ARM_MOI = 1.2;
    
    // Reduction between motors and encoder, as output over input. If the arm spins slower than
    // the motors, this number should be greater than one.
    private static final double ARM_GEARING = 10.0;
    
    private final TrapezoidProfile profile =
            new TrapezoidProfile(
                    new TrapezoidProfile.Constraints(
                            Units.degreesToRadians(45),
                            Units.degreesToRadians(90))); // Max arm speed and acceleration.
    private TrapezoidProfile.State lastProfiledReference = new TrapezoidProfile.State();
    
    // The plant holds a state-space model of our arm. This system has the following properties:
    //
    // States: [position, velocity], in radians and radians per second.
    // Inputs (what we can "put in"): [voltage], in volts.
    // Outputs (what we can measure): [position], in radians.
    private final LinearSystem<N2, N1, N1> armPlant =
            LinearSystemId.createSingleJointedArmSystem(DCMotor.getNEO(2), ARM_MOI, ARM_GEARING);
    
    // The observer fuses our encoder data and voltage inputs to reject noise.
    private final KalmanFilter<N2, N1, N1> observer =
            new KalmanFilter<>(
                    Nat.N2(),
                    Nat.N1(),
                    armPlant,
                    VecBuilder.fill(0.015, 0.17), // How accurate we
                    // think our model is, in radians and radians/sec
                    VecBuilder.fill(0.01), // How accurate we think our encoder position
                    // data is. In this case we very highly trust our encoder position reading.
                    0.020);
    
    // A LQR uses feedback to create voltage commands.
    private final LinearQuadraticRegulator<N2, N1, N1> controller =
            new LinearQuadraticRegulator<>(
                    armPlant,
                    VecBuilder.fill(Units.degreesToRadians(1.0), Units.degreesToRadians(10.0)), // QELMs.
                    // Position and velocity error tolerances, in radians and radians per second. Decrease
                    // this
                    // to more heavily penalize state excursion, or make the controller behave more
                    // aggressively. In this example we weight position much more highly than velocity, but
                    // this
                    // can be tuned to balance the two.
                    VecBuilder.fill(12.0), // RELMs. Control effort (voltage) tolerance. Decrease this to more
                    // heavily penalize control effort, or make the controller less aggressive. 12 is a good
                    // starting point because that is the (approximate) maximum voltage of a battery.
                    0.020); // Nominal time between loops. 0.020 for TimedRobot, but can be
    // lower if using notifiers.
    
    // The state-space loop combines a controller, observer, feedforward and plant for easy control.
    private final LinearSystemLoop<N2, N1, N1> loop =
            new LinearSystemLoop<>(armPlant, controller, observer, 12.0, 0.020);
    
    // An encoder set up to measure arm position in radians.
    private final Encoder encoder = new Encoder(ENCODER_A_CHANNEL, ENCODER_B_CHANNEL);
    
    private final MotorController motor = new PWMSparkMax(MOTOR_PORT);
    
    // A joystick to read the trigger from.
    private final Joystick joystick = new Joystick(JOYSTICK_PORT);
    
    
    @Override
    public void robotInit()
    {
        // We go 2 pi radians in 1 rotation, or 4096 counts.
        encoder.setDistancePerPulse(Math.PI * 2 / 4096.0);
    }
    
    
    @Override
    public void teleopInit()
    {
        // Reset our loop to make sure it's in a known state.
        loop.reset(VecBuilder.fill(encoder.getDistance(), encoder.getRate()));
        
        // Reset our last reference to the current state.
        lastProfiledReference =
                new TrapezoidProfile.State(encoder.getDistance(), encoder.getRate());
    }
    
    
    @Override
    public void teleopPeriodic()
    {
        // Sets the target position of our arm. This is similar to setting the setpoint of a
        // PID controller.
        TrapezoidProfile.State goal;
        if (joystick.getTrigger())
        {
            // the trigger is pressed, so we go to the high goal.
            goal = new TrapezoidProfile.State(RAISED_POSITION, 0.0);
        }
        else
        {
            // Otherwise, we go to the low goal
            goal = new TrapezoidProfile.State(LOWERED_POSITION, 0.0);
        }
        // Step our TrapezoidalProfile forward 20ms and set it as our next reference
        lastProfiledReference = profile.calculate(0.020, lastProfiledReference, goal);
        loop.setNextR(lastProfiledReference.position, lastProfiledReference.velocity);
        // Correct our Kalman filter's state vector estimate with encoder data.
        loop.correct(VecBuilder.fill(encoder.getDistance()));
        
        // Update our LQR to generate new voltage commands and use the voltages to predict the next
        // state with out Kalman filter.
        loop.predict(0.020);
        
        // Send the new calculated voltage to the motors.
        // voltage = duty cycle * battery voltage, so
        // duty cycle = voltage / battery voltage
        double nextVoltage = loop.getU(0);
        motor.setVoltage(nextVoltage);
    }
}
