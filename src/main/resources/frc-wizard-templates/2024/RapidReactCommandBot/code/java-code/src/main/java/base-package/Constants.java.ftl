<#ftl output_format="plainText" encoding="UTF-8">
<#--noinspection WrongPackageStatement-->
<#compress>
<#-- @ftlvariable name="data" type="net.javaru.iip.frc.wizard.FrcProjectWizardData" -->
<#--
    Template Language Reference: https://freemarker.apache.org/docs/ref.html
    Template Author's Guide:     https://freemarker.apache.org/docs/dgui.html
-->
<#--  To DEBUG templates, set system property 'frc.freemarker.debug' to true when launching the testing instance of IntelliJ IDEA -->
</#compress>
${data.copyright}

package ${data.basePackage};

import edu.wpi.first.math.util.Units;



/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants
{
    public static final class DriveConstants
    {
        public static final int LEFT_MOTOR_1_PORT = 0;
        public static final int LEFT_MOTOR_2_PORT = 1;
        public static final int RIGHT_MOTOR_1_PORT = 2;
        public static final int RIGHT_MOTOR_2_PORT = 3;
        
        public static final int[] LEFT_ENCODER_PORTS = {0, 1};
        public static final int[] RIGHT_ENCODER_PORTS = {2, 3};
        public static final boolean LEFT_ENCODER_REVERSED = false;
        public static final boolean RIGHT_ENCODER_REVERSED = true;
        
        public static final int ENCODER_CPR = 1024;
        public static final double WHEEL_DIAMETER_METERS = Units.inchesToMeters(6);
        public static final double ENCODER_DISTANCE_PER_PULSE =
                // Assumes the encoders are directly mounted on the wheel shafts
                (WHEEL_DIAMETER_METERS * Math.PI) / (double) ENCODER_CPR;
    }
    
    public static final class ShooterConstants
    {
        public static final int[] ENCODER_PORTS = {4, 5};
        public static final boolean ENCODER_REVERSED = false;
        public static final int ENCODER_CPR = 1024;
        public static final double ENCODER_DISTANCE_PER_PULSE =
                // Distance units will be rotations
                1.0 / (double) ENCODER_CPR;
        
        public static final int SHOOTER_MOTOR_PORT = 4;
        public static final int FEEDER_MOTOR_PORT = 5;
        
        public static final double SHOOTER_FREE_RPS = 5300;
        public static final double SHOOTER_TARGET_RPS = 4000;
        public static final double SHOOTER_TOLERANCE_RPS = 50;
        
        // These are not real PID gains, and will have to be tuned for your specific robot.
        public static final double P = 1;
        
        // On a real robot the feedforward constants should be empirically determined; these are
        // reasonable guesses.
        public static final double S_VOLTS = 0.05;
        public static final double V_VOLT_SECONDS_PER_ROTATION =
                // Should have value 12V at free speed...
                12.0 / SHOOTER_FREE_RPS;
        
        public static final double FEEDER_SPEED = 0.5;
    }
    
    public static final class IntakeConstants
    {
        public static final int MOTOR_PORT = 6;
        public static final int[] SOLENOID_PORTS = {2, 3};
    }
    
    public static final class StorageConstants
    {
        public static final int MOTOR_PORT = 7;
        public static final int BALL_SENSOR_PORT = 6;
    }
    
    public static final class AutoConstants
    {
        public static final double TIMEOUT_SECONDS = 3;
        public static final double DRIVE_DISTANCE_METERS = 2;
        public static final double DRIVE_SPEED = 0.5;
    }
    
    public static final class OIConstants
    {
        public static final int DRIVER_CONTROLLER_PORT = 0;
    }
}
