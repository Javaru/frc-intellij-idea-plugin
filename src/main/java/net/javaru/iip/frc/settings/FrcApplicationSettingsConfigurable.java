/*
 * Copyright 2015-2023 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *     
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package net.javaru.iip.frc.settings;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.options.SearchableConfigurable;
import net.javaru.iip.frc.i18n.FrcPluginConfigBundle;
import net.javaru.iip.frc.settings.forms.FrcApplicationSettingsForm;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;



// This class is registered in the plugin.xml as an <applicationConfigurable>
public class FrcApplicationSettingsConfigurable implements SearchableConfigurable 
{
    public static final String ID = "preferences.language.FRC"; // Needs to match the is in the plugin.xml <applicationConfigurable>
    
    private static final Logger LOG = Logger.getInstance(FrcApplicationSettingsConfigurable.class);

    private FrcApplicationSettingsForm myForm;


    @NotNull
    public static FrcApplicationSettingsConfigurable getInstance()
    {
        final FrcApplicationSettingsConfigurable instance = ApplicationManager.getApplication().getComponent(FrcApplicationSettingsConfigurable.class);
        return instance != null ? instance : new FrcApplicationSettingsConfigurable();
    }
    
    @Nls
    @Override
    public String getDisplayName()
    {
        // Note that this message in the Plugin Config Bundle (not the standard FrcBundle) since it is also used
        // in the 'key' attribute of the applicationConfigurable (for this class) in the plugin.xml 
        return FrcPluginConfigBundle.message("frc.application.settings.display.name");
    }


    @Nullable
    @Override
    public String getHelpTopic() { return null; }


    @Nullable
    @Override
    public JComponent createComponent()
    {
        myForm = new FrcApplicationSettingsForm();
        return myForm.getRootPanel();
    }


    @Override
    public boolean isModified()
    {
        return myForm != null && myForm.isModified();
    }


    @Override
    public void apply() throws ConfigurationException
    {
        myForm.applyTo(FrcApplicationSettings.getInstance());
    }


    @Override
    public void reset() { myForm.resetForm(); }


    @Override
    public void disposeUIResources() { myForm = null; }


    @NotNull
    @Override
    public String getId() // Specified in SearchableConfigurable
    {
        return ID;
    }
}
