/*
 * Copyright 2015-2022 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *     
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package net.javaru.iip.frc.riolog.udp;

import java.io.IOException;
import java.net.BindException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.BooleanUtils;
import org.jetbrains.annotations.NotNull;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;

import net.javaru.iip.frc.riolog.AbstractRioLogMonitorProcess;



public class UdpRioLogMonitorProcess extends AbstractRioLogMonitorProcess
{
    private static final Logger LOG = Logger.getInstance(UdpRioLogMonitorProcess.class);

    private static final boolean USE_DEBUGGING_SERVER = BooleanUtils.toBoolean(System.getProperty(SIMULATED_LOG_SERVICE_ENABLED_PROP_KEY,
                                                                                                  Boolean.FALSE.toString()));

    /**
     *
     * @param project the project the monitoring process is running for
     * @param clearConsoleRunnable Runnable that programmatically 'clicks' the clear button on the Executor window.
     * @param stopRioLogRunnable   Runnable that programmatically 'clicks' the stop button on the Executor window.
     *
     * @throws IllegalStateException If an initialization issue occurs
     */
    public UdpRioLogMonitorProcess(@NotNull Project project, @NotNull Runnable clearConsoleRunnable, @NotNull Runnable stopRioLogRunnable) throws IllegalStateException
    {
        super(project, clearConsoleRunnable, stopRioLogRunnable);
    }


    @NotNull
    @Override
    protected RioLogMonitoringRunnable initMonitoringRunnable()

    {
        RioLogMonitoringRunnable rioLogMonitor;
        if (USE_DEBUGGING_SERVER)
        {
            rioLogMonitor = new TestingUdpRioLogMonitoringRunnable();
            LOG.warn(String.format("[FRC] System Property '%s is set to 'true'. Using '%s' for monitoring on port '%d'.",
                                   SIMULATED_LOG_SERVICE_PROP_KEY_BASE,
                                   rioLogMonitor.getClass().getSimpleName(),
                                   rioLogMonitor.getPort()));
        }
        else
        {
            rioLogMonitor = new UdpRioLogMonitoringRunnable();
            final String message = rioLogMonitor.getPort() != null
                                   ? String.format("[FRC] Using '%s' for monitoring on port '%d'.",
                                                   rioLogMonitor.getClass().getSimpleName(),
                                                   rioLogMonitor.getPort())
                                   : String.format("[FRC] Using '%s' for monitoring.",
                                                   rioLogMonitor.getClass().getSimpleName());
            LOG.info(message);
        }
        return rioLogMonitor;
    }


    private class UdpRioLogMonitoringRunnable extends AbstractRioLogMonitoringRunnable
    {
    
    
        protected UdpRioLogMonitoringRunnable()
        {
            this(getSettings().getRioLogUdpPort());
        }
    
    
        public UdpRioLogMonitoringRunnable(int port)
        {
            super(port);
        }
    
    
        @Override
        public void run()
        {
            logStartingMonitoring();
    
            isRunning = true;
            byte[] buffer = new byte[MAX_PACKET_SIZE];
            try (DatagramSocket socket = createSocket())
            {
                socket.setSoTimeout((int) TimeUnit.SECONDS.toMillis(1)); // check every x seconds for shutdown
    
                DatagramPacket incomingPacket;
    
                while (enabled)
                {
                    incomingPacket = new DatagramPacket(buffer, buffer.length);
                    try
                    {
                        socket.receive(incomingPacket);
                        final String received = new String(incomingPacket.getData(), 0, incomingPacket.getLength());
                        processReceivedText(received);
                    }
                    catch (SocketTimeoutException ignore)
                    {
                        if (!enabled)
                        {
                            isRunning = false;
                            consoleWriter.flush();
                            if (fileWriter != null)
                            {
                                fileWriter.flush();
                            }
                            cleanUpSocket(socket);
                            return;
                        }
                    }
                    catch (IOException e)
                    {
                        LOG.warn("[FRC] An IOException occurred while monitoring the RIO Log UDP output", e);
                        isRunning = false;
                        cleanUpSocket(socket);
                        return;
                    }
                }
            }
            catch (BindException e)
            {
                final String msg = "Could not bind to the RioLog port. This can occur if the port is already bound to in another "
                                   + "instance of IntelliJ IDEA with an FRC project open, or by another tool, such as Eclipse. "
                                   + "You will need to stop the RioLog monitoring in the other tool and then then reattempt to start monitoring.";
                LOG.warn(msg + " Cause Summary: " + e.toString(), e);
                consoleWriter.println();
                consoleWriter.println("==Could not bind to the RioLog UDP port.==");
                consoleWriter.println(
                        "This can occur if the port is already bound to in another instance of IntelliJ IDEA with an FRC project open, or by another tool, such as Eclipse.");
                consoleWriter.println("You will need to stop the monitoring in the other tool and then reattempt to start monitoring.");
                consoleWriter.println(
                        "Since this UDP based RIOLog monitoring is no longer used (since 2018), and this is considered legacy feature, there is no plan to resolve this minor shortcoming of only having a single monitor running at a time.");
                consoleWriter.println();
                consoleWriter.flush();
                // publishBindWarning(msg);
                isRunning = false;
            }
            catch (Exception e)
            {
                LOG.warn("[FRC] An Exception occurred while monitoring the RIO Log UDP output", e);
                isRunning = false;
            }
            //This sets isRunning to false after the while(enabled) loop exits so the destroy method knows its ok to exit
            isRunning = false;
        }
    
    
        @Override
        @NotNull
        protected String getStartingMonitoringMessage()
        {
            StringBuilder sb = new StringBuilder();
            sb.append(get2018ChangeMessage());
            sb.append("==Monitoring RioLog");
            if (getPort() != null)
            {
                sb.append(" on port ").append(getPort());
            }
            sb.append("==");
            return sb.toString();
        }
    
    
        protected DatagramSocket createSocket() throws IOException
        {
            if (getPort() == null)
            {
                throw new IllegalStateException("port is null");
            }
            // We try to create the socket doing a retry after 1 second on the first BindException
            LOG.debug("[FRC] Creating DatagramSocket with port " + getPort());
            DatagramSocket socket;
            try
            {
                socket = new DatagramSocket(getPort());
            }
            catch (BindException e)
            {
                try {TimeUnit.SECONDS.sleep(1);} catch (InterruptedException ignore) {}
                socket = new DatagramSocket(getPort());
            }
            socket.setReuseAddress(true);
            socket.setBroadcast(true);
            return socket;
        }
    
    
        protected void cleanUpSocket(DatagramSocket socket) throws IOException
        {
            //no op - here mostly for the testing version of this class
        }

    }

    @NotNull
    public static String get2018ChangeMessage()
    {
        return "\n"
               + "**************************************************************************************************************************************************\n" 
               + "I M P O R T A N T   N O T I C E \n" 
               + "WPILib changed the RIOLog implementation for 2018. It was changed from using UDP to a new and more robust TCP based implementation.\n" 
               + "This Legacy Net Console uses the older UDP based monitoring and will only work with Robots running v2017 or older WPILib code on the roboRIO.\n" 
               + "To access the current Net Console go to the menu:  Tools > FRC > RIOLog Monitoring > Net Console \n"
               + "The alternate 'ssh tailing' monitoring can also be used with all robots. From the menu:  Tools > FRC > RIOLog Monitoring > SSH Tailing \n"
               + "I M P O R T A N T   N O T I C E \n"
               + "**************************************************************************************************************************************************\n"
               + "\n";
    }


    private class TestingUdpRioLogMonitoringRunnable extends UdpRioLogMonitoringRunnable
    {
        private final InetAddress groupAddress;


        public TestingUdpRioLogMonitoringRunnable()
        {
            super(determineTestPort());

            try
            {
                groupAddress = InetAddress.getByName("230.0.0.1");
            }
            catch (UnknownHostException e)
            {
                final String message = "Cannot create group InetAddress due to an exception.";
                LOG.warn("[FRC] " + message);
                throw new IllegalStateException(message, e);
            }
        }


        @Override
        protected DatagramSocket createSocket() throws IOException
        {
            if (getPort() == null)
            {
                throw new IOException("port is null");
            }
                
            MulticastSocket socket = new MulticastSocket(getPort());
            socket.joinGroup(groupAddress);
            return socket;
        }


        @Override
        protected void cleanUpSocket(DatagramSocket socket) throws IOException
        {
            if (socket instanceof MulticastSocket)
            {
                ((MulticastSocket) socket).leaveGroup(groupAddress);
            }
        }


        @NotNull
        @Override
        protected String getStartingMonitoringMessage() { return "«««Monitoring *SIMULATED* RioLog on port " + getPort() + "»»»"; }


        @Override
        protected boolean addLineBreak()
        {
            return true;
        }


    }


    private static int determineTestPort()
    {
        boolean useConfiguredPort = BooleanUtils.toBoolean(System.getProperty(SIMULATED_LOG_SERVICE_USE_CONFIGURED_PORT_PROP_KEY, "false"));
        if (useConfiguredPort)
        {
            return getSettings().getRioLogUdpPort();
        }
        else
        {
            return Integer.valueOf(System.getProperty(SIMULATED_LOG_SERVICE_PORT_PROP_KEY, Integer.toString(SIMULATED_LOG_SERVICE_PORT_DEFAULT)));
        }
    }
}
