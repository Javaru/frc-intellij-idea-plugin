/*
 * Copyright 2015-2018 the original author or authors
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *     
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package net.javaru.iip.frc.riolog.tcp.message;

import com.intellij.openapi.diagnostic.Logger;



/* 
    "Based on" (well mostly directly ported over from) the DummyMessage class
    https://github.com/wpilibsuite/riolog/blob/master/src/main/java/netconsole2/DummyMessage.java
    https://github.com/wpilibsuite/EclipsePlugins/blob/master/edu.wpi.first.wpilib.plugins.riolog/src/netconsole2/DummyMessage.java
*/
public class DummyMessage
{
    private static final Logger LOG = Logger.getInstance(DummyMessage.class);


}
