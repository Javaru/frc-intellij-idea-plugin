/*
 * Copyright 2015-2021 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *     
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package net.javaru.iip.frc.ui.internal;

import javax.swing.*;

import org.jetbrains.annotations.Nullable;
import com.intellij.openapi.project.Project;

import io.javaru.iip.common.ui.PlaceholderTextField;



@SuppressWarnings({"unused", "UseOfConcreteClass"})
public class PlaceholderTextFieldPaddingDemoForm
{
    
    @SuppressWarnings("FieldCanBeLocal")
    private final Project project;
    
    
    public PlaceholderTextFieldPaddingDemoForm(@Nullable Project project)
    {
        this.project = project;
    }
    
    
    private JPanel rootPanel;
    private PlaceholderTextField placeholderTextField0;
    private PlaceholderTextField placeholderTextField1;
    private PlaceholderTextField placeholderTextField2;
    private PlaceholderTextField placeholderTextField3;
    private PlaceholderTextField placeholderTextField4;
    private PlaceholderTextField placeholderTextField5;
    private PlaceholderTextField placeholderTextField6;
    private PlaceholderTextField placeholderTextField7;
    private PlaceholderTextField placeholderTextField8;
    private PlaceholderTextField placeholderTextField9;
    private PlaceholderTextField placeholderTextField10;
    private PlaceholderTextField placeholderTextField11;
    private PlaceholderTextField placeholderTextField12;
    private PlaceholderTextField placeholderTextField13;
    private PlaceholderTextField placeholderTextField14;
    private PlaceholderTextField placeholderTextField15;
    private PlaceholderTextField placeholderTextField16;
    private PlaceholderTextField placeholderTextFieldDefault;
    
    
    public JPanel getRootPanel() {return rootPanel;}
    
}
