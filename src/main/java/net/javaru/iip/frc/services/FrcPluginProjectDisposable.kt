/*
 * Copyright 2015-2022 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *     
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package net.javaru.iip.frc.services

import com.intellij.openapi.Disposable
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.diagnostic.trace
import com.intellij.openapi.project.Project
import java.util.concurrent.atomic.AtomicBoolean

/**
 * A Project Service that we can use as a Project level parent disposable.
 *
 * This can easily be obtained via [net.javaru.iip.frc.util.getApplicationParentDisposable] and/or
 * [net.javaru.iip.frc.util.getParentDisposable] in `FrcPluginUtils.kt`.
 *
 * See https://jetbrains.org/intellij/sdk/docs/basics/disposers.html#choosing-a-disposable-parent
 *
 * "For resources required for the entire lifetime of a plugin, use an application or project level service."
 *
 * Based on `org.jetbrains.kotlin.idea.core.KotlinPluginDisposable`
 */
class FrcPluginProjectDisposable(val project: Project) : Disposable
{
    private val logger = logger<FrcPluginProjectDisposable>()

    private val disposedFlag = AtomicBoolean(false)
    
    val isDisposed
        get() = disposedFlag.get()

    companion object
    {
        @JvmStatic
        fun getInstance(project: Project) = project.service<FrcPluginProjectDisposable>()
    }

    override fun dispose()
    {
        try
        {
            logger.trace { "[FRC] FrcPluginProjectDisposable.dispose() called for project '$project'" }
            disposedFlag.set(true)
        }
        catch (ignore: Exception) { }
    }
}