/*
 * Copyright 2015-2021 the original author or authors.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *     
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package net.javaru.iip.frc.freemarker

import freemarker.cache.NullCacheStorage
import freemarker.template.Configuration
import freemarker.template.TemplateExceptionHandler
import icons.FrcIcons
import net.javaru.iip.frc.FrcPluginGlobals
import net.javaru.iip.frc.util.FrcSystemConfigs

// todoc document this System property
val DEBUG_MODE = FrcSystemConfigs.FreemarkerDebugEnabled.value

const val FM_TEMPLATE_EXT_NO_DOT = "ftl"
const val FM_TEMPLATE_EXT_WITH_DOT = ".$FM_TEMPLATE_EXT_NO_DOT"
const val KOTLIN_FM_TEMPLATE_FILE_EXT = ".kt$FM_TEMPLATE_EXT_WITH_DOT"
const val KOTLIN_SCRIPT_FM_TEMPLATE_FILE_EXT = ".kts$FM_TEMPLATE_EXT_WITH_DOT"

/**
 * @param resourceLoaderClass a class to be used for resource loading. This basically just needs to be any class within the plugin.
 *                            We may have to rethink this when we implement templates via extension points
 *                            
 * @param basePackagePath the base package for loading of templates. Separate steps with `/` `.` and note that it matters if this starts with
 *            `/` or not: f it doesn't start with a / then it's relative to the path (package) of the resourceLoaderClass class. If it starts 
 *            with / then it's relative to the root of the package hierarchy. Note that path components should be separated by forward slashes 
 *            independently of the separator character used by the underlying operating system.
 *            
 */
@JvmOverloads
fun freemarkerConfiguration(resourceLoaderClass:  Any, basePackagePath: String = "/"): Configuration = freemarkerConfiguration(basePackagePath, resourceLoaderClass::class.java)

/**
 * @param resourceLoaderClass a class to be used for resource loading. This basically just needs to be any class within the plugin.
 *                            We may have to rethink this when we implement templates via extension points
 *                            
 * @param basePackagePath the base package for loading of templates. Separate steps with `/` `.` and note that it matters if this starts with
 *            `/` or not: f it doesn't start with a / then it's relative to the path (package) of the resourceLoaderClass class. If it starts 
 *            with / then it's relative to the root of the package hierarchy. Note that path components should be separated by forward slashes 
 *            independently of the separator character used by the underlying operating system.
 *            
 */
@JvmOverloads
fun freemarkerConfiguration(basePackagePath: String = "/", resourceLoaderClass: Class<*> = FrcIcons::class.java): Configuration
{
    val cfg = Configuration(Configuration.VERSION_2_3_31)
    // we just need a class on our classpath, so we use FrcIcons as a convenient class
    cfg.setClassForTemplateLoading(resourceLoaderClass, basePackagePath)
    cfg.defaultEncoding = "UTF-8"
    // Sets how errors will appear.
    cfg.templateExceptionHandler = if (DEBUG_MODE) TemplateExceptionHandler.DEBUG_HANDLER else TemplateExceptionHandler.RETHROW_HANDLER
    // Sets if exceptions are logged in the processed templates 
    cfg.logTemplateExceptions = DEBUG_MODE
    // We clear cache as we do not want any stale templates, and since we only create the config one time, there is no significant penalty.
    cfg.cacheStorage?.clear()

    if (DEBUG_MODE || FrcPluginGlobals.IS_IN_FRC_INTERNAL_MODE)
    {
        // Turn off caching when debugging & testing
        FrcPluginGlobals.GENERAL_LOGGER.info("[FRC] Disabling Freemarker Caching/Cache")
        cfg.cacheStorage = NullCacheStorage() // turn off caching
    }

    return cfg
}

/**
 * Creates a Freemarker [Configuration] with interpolation syntax set to Square bracket syntax.
 * This allows us to use `[=data.robotClassSimpleName]` rather than `${data.robotClassSimpleName}`,
 * and thus not conflict with Kotlin String template syntax. Note that this only affects interpolation
 * syntax, and *NOT* Tag syntax. So we will still use `<#if isSuchAndSuch>` and not `[#if isSuchAndSuch]`.
 * Tag syntax can be changed if desired, but we are not.
 *
 * @param resourceLoaderClass a class to be used for resource loading. This basically just needs to be any class within the plugin.
 *                            We may have to rethink this when we implement templates via extension points
 *
 * @param basePackagePath the base package for loading of templates. Separate steps with `/` `.` and note that it matters if this starts with
 *            `/` or not: f it doesn't start with a / then it's relative to the path (package) of the resourceLoaderClass class. If it starts
 *            with / then it's relative to the root of the package hierarchy. Note that path components should be separated by forward slashes
 *            independently of the separator character used by the underlying operating system.
 */
@JvmOverloads
fun freemarkerConfigurationForKotlinTemplates(resourceLoaderClass: Any, basePackagePath: String = "/"): Configuration =
    freemarkerConfigurationForKotlinTemplates(basePackagePath, resourceLoaderClass::class.java)


/**
 * Creates a Freemarker [Configuration] with interpolation syntax set to Square bracket syntax.
 * This allows us to use `[=data.robotClassSimpleName]` rather than `${data.robotClassSimpleName}`,
 * and thus not conflict with Kotlin String template syntax. Note that this only affects interpolation
 * syntax, and *NOT* Tag syntax. So we will still use `<#if isSuchAndSuch>` and not `[#if isSuchAndSuch]`.
 * Tag syntax can be changed if desired, but we are not.
 *
 * @param resourceLoaderClass a class to be used for resource loading. This basically just needs to be any class within the plugin.
 *                            We may have to rethink this when we implement templates via extension points
 *
 * @param basePackagePath the base package for loading of templates. Separate steps with `/` `.` and note that it matters if this starts with
 *            `/` or not: f it doesn't start with a / then it's relative to the path (package) of the resourceLoaderClass class. If it starts
 *            with / then it's relative to the root of the package hierarchy. Note that path components should be separated by forward slashes
 *            independently of the separator character used by the underlying operating system.
 */
@JvmOverloads
fun freemarkerConfigurationForKotlinTemplates(basePackagePath: String = "/", resourceLoaderClass: Class<*> = FrcIcons::class.java): Configuration
{
    val cfg = freemarkerConfiguration(basePackagePath, resourceLoaderClass)
    cfg.interpolationSyntax = Configuration.SQUARE_BRACKET_INTERPOLATION_SYNTAX
    return cfg
}

