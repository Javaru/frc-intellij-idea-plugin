/*
 * Copyright 2015-2019 the original author or authors
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *     
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package net.javaru.iip.frc.wpilib.version


private val WPILIB_2018_VERSION_CHECK = maxVersion(2017)

fun minVersion(year: Int) = WpiLibVersionImpl.parse("${year}.0.0")
fun maxVersion(year: Int) = WpiLibVersionImpl.parse("${year}.999999.0")
fun firstRelease(year: Int) = WpiLibVersionImpl.parse("${year}.1.1")

/** Checks if a WpiLibVersion is a 2018 Project. (Was needed to be aware of a Ant build change made in 2018, but is not applicable to the new 2019 GradleRIO builds.) */
fun WpiLibVersion.is2018Project(): Boolean = this.isNewerThan(WPILIB_2018_VERSION_CHECK) && !this.usesGradle()

fun WpiLibVersion.usesGradle(): Boolean = this.generation >= 2019

fun WpiLibVersion.usesAnt(): Boolean = this.generation < 2019